using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] GameObject FollowObject;
    [SerializeField] GameObject CameraPosition;
    [SerializeField] Vector3 offset;
    [SerializeField] float Speed;

    // Update is called once per frame
    void FixedUpdate()
    {
        Follow();
    }

    private void Follow()
    {
        gameObject.transform.position = Vector3.Lerp(transform.position, CameraPosition.transform.position, Time.deltaTime * Speed);
        gameObject.transform.LookAt(FollowObject.transform.position + offset);
    }
}
