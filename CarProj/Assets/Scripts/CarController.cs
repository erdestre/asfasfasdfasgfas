using UnityEngine;

public class CarController : MonoBehaviour
{
    //left, right, gas, brake, handbrake
    //SpinWheel, SteerWheel, Accelerate
    [Header("Car Features")]
    [SerializeField] bool isFWD, isBWD;
    [SerializeField] float MotorPower;
    [SerializeField] float WheelSteer;
    [SerializeField] float HandBrakeTorque = 600f;
    [SerializeField] float DefaultExtremumSlip = 0.3f;
    [SerializeField] float HandBrakeExtremumSlip = 2f;

    [Header("Car Mechanics")]
    public WheelCollider CRightFront, CLeftFront;
    [SerializeField] WheelCollider CRightRear, CLeftRear;
    [SerializeField] Transform TRightFront, TLeftFront, TRightRear, TLeftRear;
    [SerializeField] Transform StartingPoint;
    
    float HorizontalInput;
    float VerticalInput;
    bool HandBrake;
    
    public float _VerticalInput
{
        set => VerticalInput = value;
    }
    public float _HorizontalInput
    {
        set => HorizontalInput = value;
    }
    public bool _HandBrake
    {
        set => HandBrake = value;
    }

    private void Start()
    {
        if (Application.isPlaying)
        {
            GameObject Canvas = GameObject.Find("AndroidCanvas");
            Canvas.SetActive(false);
        }
    }


    private void GetInputs()
    {
        if (Application.isPlaying)
        {
            HorizontalInput = InputManager.MainHorizontal();
            VerticalInput = InputManager.MainVertical();
            if (InputManager.HandBrakeDown())
            {
                HandBrake = true;
            }
            if (InputManager.HandBrakeUp())
            {
                HandBrake = false;
            }
        }
       
    }
    private void Steer()
    {
       CLeftFront.steerAngle = HorizontalInput * WheelSteer;
       CRightFront.steerAngle = HorizontalInput * WheelSteer;
    }
    private void Accelerate()
    {
            CLeftFront.motorTorque = VerticalInput * MotorPower;
            CRightFront.motorTorque = VerticalInput * MotorPower;
            CRightRear.motorTorque = VerticalInput * MotorPower;
            CLeftRear.motorTorque = VerticalInput * MotorPower;

    }
    private void WheelAnimation()
    {
        ChangeWheelPose(CLeftFront,TLeftFront);
        ChangeWheelPose(CLeftRear, TLeftRear);
        ChangeWheelPose(CRightFront, TRightFront);
        ChangeWheelPose(CRightRear, TRightRear);
    }
    
    private void ChangeWheelPose(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 wheelPosition = wheelCollider.transform.position;
        Quaternion wheelRotation = wheelCollider.transform.rotation;

        wheelCollider.GetWorldPose(out wheelPosition, out wheelRotation);

        wheelTransform.position = wheelPosition;
        wheelTransform.rotation = wheelRotation;
    }
    private void Update()
    {
        GetInputs();
        
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Steer();
        Accelerate();
        WheelAnimation();
        ActivateHandBrake();

        CarBehaviour();

    }
    private void CarBehaviour()
    {

        if (HandBrake)
        {
            WheelFrictionCurve myWfc;
            myWfc = CLeftFront.sidewaysFriction;
            myWfc.extremumSlip = HandBrakeExtremumSlip;
            myWfc.stiffness = 2.5f;

            CLeftFront.sidewaysFriction = myWfc;
            CRightFront.sidewaysFriction = myWfc;

            myWfc.stiffness = myWfc.stiffness/2;
            CLeftRear.sidewaysFriction = myWfc;
            CRightRear.sidewaysFriction = myWfc;

            
        }
        else
        {
            WheelFrictionCurve myWfc;
            myWfc = CLeftFront.sidewaysFriction;
            myWfc.extremumSlip = DefaultExtremumSlip;
            myWfc.stiffness = 1f;

            CLeftFront.sidewaysFriction = myWfc;
            CLeftRear.sidewaysFriction = myWfc;
            CRightFront.sidewaysFriction = myWfc;
            CRightRear.sidewaysFriction = myWfc;
        }
    }
    private void ActivateHandBrake()
    {
        if (HandBrake)
        {
            //CLeftFront.brakeTorque = HandBrakeTorque;
            //CRightFront.brakeTorque = HandBrakeTorque;
            CLeftRear.brakeTorque = HandBrakeTorque;
            CRightRear.brakeTorque = HandBrakeTorque;

            //CLeftFront.motorTorque = 0f;
            //CRightFront.motorTorque = 0f;
            CLeftRear.motorTorque = 0f;
            CRightRear.motorTorque = 0f;
        }
        else
        {
            //CLeftFront.brakeTorque = 0f;
            //CRightFront.brakeTorque = 0f;
            CLeftRear.brakeTorque = 0f;
            CRightRear.brakeTorque = 0f;
        }
    }
    public void Resetcar()
    {
        gameObject.transform.position = StartingPoint.position;
        gameObject.transform.rotation = StartingPoint.rotation;
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
    }
}
